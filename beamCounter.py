import sys, os
import h5py
import ROOT
from ROOT import TFile, TNtuple, TROOT
import time


def beam_cnt(b,freq,maxfn):
    #beamCounts = ROOT.TH1D("e1519"+str(b)+"_beamCounts","e1519"+str(b)+" beamCounts",540,-0.1,2.6) #bins of 0.1 increment

    sumBC = 0
    tcrFlag = 0

    for fn in range(0,maxfn):
        if freq == 25 or freq == 50:
            f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/ps'+str(b)+'_'+str(freq)+'_file'+str(fn)+'.h5','r')
        else:
            f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/E1519'+str(b)+'_file'+str(fn)+'.h5','r')

        print('opening file %s ...' % f)

        for i,key in enumerate(f.keys()):
            sumBC += f[key]['beamCounts'][()]
            if f[key]['beamTCR'][()] == 1:
                tcrFlag += 1
                print(" Ooo... TCR is 1 \_o_/ ")
                if i%100==0:
                    print(i,time.strftime("%H:%M:%S"))
    
    # maxBin = tcrFlag*pow(2,32)+sumBC + 10
    # numOfBins = int(maxBin/10)
    # print(maxBin,numOfBins)
    if freq == 25 or freq == 50:
        beamCounts = ROOT.TH1D("ps"+str(b)+"_"+str(freq)+"_beamCounts","ps"+str(b)+"_"+str(freq)+" beamCounts",10,0.0,10.0) #bins of 0.1 increment 
    else:
        beamCounts = ROOT.TH1D("e1519"+str(b)+"_beamCounts","e1519"+str(b)+" beamCounts",10,0.0,10.0) #bins of 0.1 increment
    print(" filling...")
    beamCounts.Fill( tcrFlag*pow(2,32)+sumBC )

    beamCounts.SetDirectory(0)

    # outHistFile = ROOT.TFile.Open(outFileName ,"RECREATE")
    # outHistFile.cd()
    # beamCounts.Write()
    # outHistFile.Close()
    
    
    print('beam count is returning...')
    return beamCounts


