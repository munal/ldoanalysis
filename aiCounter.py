import sys, os
import h5py
import ROOT
from ROOT import TFile, TNtuple, TROOT
import time

# if len(sys.argv) != 3:
#     print("USAGE: %s <last 2 digits of board id> <channel number [1,4]>"%(sys.argv[0]))
#     sys.exit(1)


# b = int(sys.argv[1])
# ch = int(sys.argv[2])


def ai_hist(b,freq,ch,maxfn):

        #outFileName = 'e1519'+str(b)+'_ai'+str(ch)+'_histo.root'
        if freq == 25 or freq == 50:
                ai = ROOT.TH1D("ps"+str(b)+"_"+str(freq)+"_CH"+str(ch)+"_AI","ps"+str(b)+"_"+str(freq)+" CH"+str(ch)+" AI",540,-0.1,2.6)
        else:
                ai = ROOT.TH1D("e1519"+str(b)+"_CH"+str(ch)+"_AI","e1519"+str(b)+" CH"+str(ch)+" AI",540,-0.1,2.6) #bins of 0.1 increment
	
        valArr = []
	
        for fn in range(0,maxfn):
                if freq == 25 or freq == 50:
                        f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/ps'+str(b)+'_'+str(freq)+'_file'+str(fn)+'.h5','r')
                else:
                        f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/E1519'+str(b)+'_file'+str(fn)+'.h5','r')

                print('opening file %s ...' % f)
	
                for i,key in enumerate(f.keys()):
                        valArr = f[key]['AI'][()][ch-1]
                        for el in valArr:
                                ai.Fill(el)
                        if i%100==0:
                                print(i,time.strftime("%H:%M:%S"))
		
        ai.SetDirectory(0)
	
	# outHistFile = ROOT.TFile.Open(outFileName ,"RECREATE")
	# outHistFile.cd()
	# ai.Write()
	# outHistFile.Close()
	
	
        print('ai is returning...')
        
        return ai



