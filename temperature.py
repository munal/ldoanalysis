import sys, os
import h5py
import ROOT
from ROOT import TFile, TNtuple, TROOT
import time
import math

def temperature(b,freq,maxfn):
    if freq == 25 or freq == 50:
        temp_hist = ROOT.TH1D("ps"+str(b)+"_"+str(freq)+"_temperature","ps"+str(b)+"_"+str(freq)+" temperature",57,-70,500)
    else:
        temp_hist = ROOT.TH1D("e1519"+str(b)+"_temperature","e1519"+str(b)+" temperature",57,-70,500) #bins of 0.1 increment

    for fn in range(0,maxfn):
        if freq == 25 or freq == 50:
            f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/ps'+str(b)+'_'+str(freq)+'_file'+str(fn)+'.h5','r')
        else:
            f = h5py.File('/data/munal/LDOruns/20200309_LDODryRuns/E1519'+str(b)+'_file'+str(fn)+'.h5','r')
        print('opening file %s ...' % f)

        for i,key in enumerate(f.keys()):
            RVal = f[key]['RTD4WRES'][()]
            TVal = -( (math.sqrt(-0.00232*RVal + 17.59246) - 3.908 ) / 0.00116 )
            #print(RVal,TVal)
            if i%100==0:
                print(i,time.strftime("%H:%M:%S"))

    print(" filling...")
    temp_hist.Fill( TVal )

    temp_hist.SetDirectory(0)
    
    print('temperature is returning...')
    return temp_hist


