from aiCounter import *
from beamCounter import *
from temperature import *

#def main():
if len(sys.argv) != 2:
        print("USAGE: %s <last 2 digits of board id> <channel number [1,4]>"%(sys.argv[0]))                                                
        sys.exit(1)                                                                                                                                                      
b = int(sys.argv[1])                                                        
#ch = int(sys.argv[2])  # if len(sys.argv) != 3:

outFileName_ai = 'e1519'+str(b)+'_ai_hist.root'	
outHistFile_ai = ROOT.TFile.Open(outFileName_ai ,"RECREATE")

#AI histograms
for ch in range(1,5):
        print(b,ch)
        ai = ai_hist(b,ch)
	
        outHistFile_ai.cd()
        ai.Write()
outHistFile_ai.Close()	


#Beam Counter 
outFileName_bc = 'e1519'+str(b)+'_beamCounts_hist.root'
outHistFile_bc = ROOT.TFile.Open(outFileName_bc ,"RECREATE")
beamCounts = beam_cnt(b)
outHistFile_bc.cd()
beamCounts.Write()
outHistFile_bc.Close()


#Temperature 
outFileName_temp = 'e1519'+str(b)+'_temperature_hist.root'
outHistFile_temp = ROOT.TFile.Open(outFileName_temp ,"RECREATE")
temp_hist = temperature(b)
outHistFile_temp.cd()
temp_hist.Write()
outHistFile_temp.Close()


# if __name__ == "__main__":
#         main()
